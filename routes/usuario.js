/*
const express = require("express");

const bcrypt = require("bcrypt");

const Usuario = require("../models/usuario");

const app = express();

app.post("/usuario", function (req, res) {
  let body = req.body;

  let usuario = new Usuario({
    nombre: body.nombre,
    email: body.email,
    password: bcrypt.hashSync(body.password, 10),
    role: body.role,
  });

  usuario.save((err, usuarioDB) => {
    if (err) {
      return res.status(400).json({
        ok: false,
        err,
      });
    }

    usuarioDB.password = null;

    res.json({
      ok: true,
      usuario: usuarioDB,
    });
  });
});

app.get("/usuario", function (req, res) {
  Usuario.find({}).exec((err, usuarios) => {
    if (err) {
      return res.status(400).json({
        ok: false,
        err,
      });
    }
    res.json({
      ok: true,
      usuarios,
    });
  });
});


app.put("/usuario/:id", function (req, res) {
  let id = req.params.id;

  let body = req.body;

  Usuario.findByIdAndUpdate(id, body, { new: true }, (err, usuarioDB) => {
    if (err) {
      return res.status(400).json({
        ok: false,
        err,
      });
    }

    res.status(200).json({
      ok: true,
      usuario: usuarioDB,
    });
  });
});

app.delete("/usuario/:id", function (req, res) {
  let id = req.params.id;

  Usuario.findByIdAndRemove(id, (err, usuarioBorrado) => {
    if (err) {
      return res.status(400).json({
        ok: false,
        err,
      });
    }
    res.json({
      ok: true,
      usuario: usuarioBorrado,
    });
  });
});

module.exports = app;
*/

///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
/*
const express = require("express");

const bodyParser = require('body-parser');
const bcrypt = require("bcrypt");
const path = require("path");

const Usuario = require("../models/usuario");

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


exports.usuarioPost = function (req, res) {
    let body = req.body;

    let usuario = new Usuario({
        nombre: body.nombre,
        email: body.email,
        password: bcrypt.hashSync(body.password, 10),
        role: body.role,
    });

    usuario.save((err, usuarioDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err,
            });
        }

        usuarioDB.password = null;

        res.json({
            ok: true,
            usuario: usuarioDB,
        });
    });
};

exports.usuarioGet = function (req, res){
    Usuario.find({}).exec((err, usuarios) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }
        res.json({
            usuarios
        });
    });
};

exports.usuarioPut = function (req, res) {
    let id = req.params.id;

    let body = req.body;
    var pass = body.password;
    body.password = bcrypt.hashSync(pass, 10);

    Usuario.findByIdAndUpdate(id, body, { new: true }, (err, usuariosDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.status(200).json({
            ok: true
            //usuarios: usuariosDB,
        });
    });
};

exports.usuarioDelete = function (req, res) {
    let id = req.params.id;

    Usuario.findByIdAndRemove(id, (err, usuarioBorrado) => {
        if (err) {
            return res.status(400).json({
                resultado: 'error',
                err
            });
        }
        if (usuarioBorrado==null){
          return res.json({resultado: 'inexistente'});
        }else return res.json({resultado: 'ok'});
    });
};

exports.index = function (req, res) {
    res.sendFile(path.join(__dirname+'/../Views/home.html'));
};

exports.validarCrear = function(req,res){
    let body = req.body;

    let usuario = new Usuario({
        nombre: body.nombre,
        email: body.email,
        password: bcrypt.hashSync(body.password, 10),
        role: body.role
    });

    Usuario.find({email: usuario.email}).exec((err, usuarios) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        if (usuarios.length==0){
            usuario.save((err, usuarioDB) => {
                if (err) {
                    return res.status(400).json({
                        ok: false,
                        err
                    });
                }
        
                usuarioDB.password = null;
        
                return res.sendFile(path.join(__dirname+'/../Views/operacionOK.html'));
            });
        }else{
            return res.sendFile(path.join(__dirname+'/../Views/errorEmail.html'))
        }

    });    
}
*/
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////


const express = require("express");

const bodyParser = require('body-parser');
const bcrypt = require("bcrypt");
const path = require("path");

const Usuario = require("../models/usuario");

const {verificaToken} = require('../middlewares/authentication')

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post('/usuario', function (req, res){
  let body = req.body;

  let usuario = new Usuario({
      nombre: body.nombre,
      email: body.email,
      password: bcrypt.hashSync(body.password, 10),
      role: body.role,
  });

  usuario.save((err, usuarioDB) => {
      if (err) {
          return res.status(400).json({
              ok: false,
              err,
          });
      }

      usuarioDB.password = null;

      res.json({
          ok: true,
          usuario: usuarioDB,
      });
  });
});


app.get("/usuario", verificaToken, (req, res) => {
  
  Usuario.find({}).exec((err, usuarios) => {
    if (err) {
        return res.status(400).json({
            ok: false,
            err,
        });
    }
    res.json({
      ok: true,
      usuarios,
    });
  });
});

app.put('/usuario/:id', function (req, res){
  let id = req.params.id;

    let body = req.body;
    var pass = body.password;
    body.password = bcrypt.hashSync(pass, 10);

    Usuario.findByIdAndUpdate(id, body, { new: true }, (err, usuariosDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.status(200).json({
            ok: true
            //usuarios: usuariosDB,
        });
    });
});

app.delete('/usuario/:id', function (req, res){
  let id = req.params.id;

    Usuario.findByIdAndRemove(id, (err, usuarioBorrado) => {
        if (err) {
            return res.status(400).json({
                resultado: 'error',
                err
            });
        }
        if (usuarioBorrado==null){
          return res.json({resultado: 'inexistente'});
        }else return res.json({resultado: 'ok'});
    });
});

app.post('/validarCrear', function (req, res){
  let body = req.body;

    let usuario = new Usuario({
        nombre: body.nombre,
        email: body.email,
        password: bcrypt.hashSync(body.password, 10),
        role: body.role
    });

    Usuario.find({email: usuario.email}).exec((err, usuarios) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        if (usuarios.length==0){
            usuario.save((err, usuarioDB) => {
                if (err) {
                    return res.status(400).json({
                        ok: false,
                        err
                    });
                }
        
                usuarioDB.password = null;
        
                return res.sendFile(path.join(__dirname+'/../Views/operacionOK.html'));
            });
        }else{
            return res.sendFile(path.join(__dirname+'/../Views/errorEmail.html'))
        }

    });
});


module.exports = app;
