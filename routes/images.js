const express = require("express");

const app = express();

const fs = require("fs");
const path = require("path");

app.get("/imagen/:tipo/:img", (req, res) => {
    let tipo = req.params.tipo;
    let img = req.params.img;

    let pathImagen = path.resolve(__dirname, '../uploads/usuarios/'+img);
    console.log("Ruta: " + pathImagen);

    if (fs.existsSync(pathImagen)) {
        res.sendFile(pathImagen);
    } else {
        let noImagePath = path.resolve(__dirname, '../public/images/no_image.png');

        res.sendFile(noImagePath);
    }
});

module.exports = app;